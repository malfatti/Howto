---
title: Home
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Willkommen im Disroot How-to-Bereich

Das Hauptziel dieses Bereichs ist es Dir zu helfen, Dich in den verschiedenen Services von **Disroot** zurecht zu finden.

Alle Services, mit allen von **Disroot** angebotenen Features, für alle Plattformen und/oder Betriebssysteme abzudecken ist ein sehr ambitioniertes und zeitfressendes Projekt, das eine Menge Arbeit erfordert. Da wir der Meinung sind, dass es vorteilhaft nicht nur für unsere Nutzer (Disrooter) sondern für die Gesamtheit der **Freie Software** und **quelloffenen** Gemeinschaften sein könnte, gleiche oder ähnliche Software zu betreiben, ist jede Hilfe von Disrootern jederzeit notwendig und willkommen.<br>
Also, wenn Du denkst, dass hier ein Tutorial fehlt, die Informationen sind nicht korrekt oder könnten verbessert werden, kontaktier uns bitte oder (noch besser) fang' an, selbst ein How-to zu schreiben,<br>

Um mehr über die verschiedenen Möglichkeiten der Mitwirkung zu erfahren, sieh Dir bitte diesen [Bereich](/contribute) an.
