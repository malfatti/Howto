---
title: Home
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Benvenuti nel sito delle guide di Disroot

L'obiettivo principale di questo sito è aiutarti a orientarti tra i vari servizi **Disroot**.

Coprire tutti i servizi, con tutte le funzionalità fornite da **Disroot**, per tutte le piattaforme e/o i sistemi operativi è un progetto molto ambizioso che richiede tanto tempo e lavoro. 
Riteniamo però che questo lavoro possa essere utile non solo per i nostri utenti (disrooter), ma per l'intera comunità del **Software libero** che utilizza gli stessi software.
Vista la mole di lavoro, qualsiasi aiuto è sempre necessario e benvenuto. <br>

Quindi se ritieni che ci sia un tutorial mancante, le informazioni non sono accurate o potrebbero essere migliorate, ti preghiamo di contattarci e (ancora meglio) iniziare a scrivere tu stesso un tutorial. <br>

Per conoscere i vari modi per contribuire, visita questa [sezione](/contribute).
