---
title: 'User Self-service Center'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: true
---

# User Self Service Center

The **User Self Service Center** is the place where you can manage your **Disroot** account settings, from changing the password to deleting it.

## Signing in
Go to [https://user.disroot.org](https://user.disroot.org) and log in with your **Disroot** username and password.

![](../en/login.png)

Once you have logged in you will see the **dashboard**.

![](../en/dashboard.png)

It is a quick access menu to the account settings and options which we will see in detail later on.

##  Reseting your Password
If for any reason, you have forgotten/lost your password, you can reset it through any of the methods described below.

![](../en/reset.gif)

1. Go to [https://user.disroot.org](https://user.disroot.org)
2. Click on the "**Forgotten Password**" button
3. Type your username and then **Search**
4. Choose one of the methods
  - **Secret Questions and Answers**: This method requires that you have previously configured the questions and that, of course, you  remember the answers. If you choose this option, you will be prompted with the questions. *Please, note that the answers are case sensitive*

  - **SMS/Email Verification**: This option allows you to request the password reset via a secondary email. This email should have been entered during the registration or prior to this process, otherwise the code will be sent to the address of the same account you are trying to reset.

!! ![](../en/note.png)<br>
!! We cannot reset your password.<br>
!! Remember that the account and its security is your responsibility so please, take all the necessary measures to keep it safe and secure.


## New User Registration
Although the process is pretty simple, we will see it step by step.

When registering an account, please keep the following in mind:

- Use the same device you fill out the form to open the verification link (otherwise, registration could fail).
- After 15 minutes the session expires and you will need to restart the process.
- If you are using TOR or a VPN and your IP has changed during the process, it will be considered as a security attack and therefore your session could get blocked.

!! It is important to note that account requests are reviwed manually on a daily basis and therefore approval may take up to 48 hours.

### Step by step registration
####  01: The User Name
This will be the name of your account and email and cannot be modified after created it. Your account name will look like _username@disroot.org_

![](../en/reg_01.png)


#### 02: The Screen Name
The screen name is the name that will be displayed in services that requires login, like **Cloud** or **Email**. Usually it is the same as your username, but you can choose anything you want.<br>
For example, your username could be _username_ and your screen name _User Name_.

![](../en/reg_02.png)


#### 03: The question
The verification question is intended to check that you are human, not a spam bot, and to keep spammers as far away as possible.<br>**Please, remember that the answer should meet the following requirements**:

- It should be at least 150 characters long.
- It must be the answer to the question, nothing more.
- It cannot be a quote or a copy/paste from an external source.
- It can be in any language, though we suggest you to use English.
- It cannot contain specific or special characters from another language than English. This means you should avoid using accents or, for example, the letter ñ.

![](../en/reg_03.png)


#### 04: The verification email
In order to complete the registration process, it is necessary to send you a verification code to a valid email. Also in this step, you can choose to use this same address for future password reset in case you need it. If you prefer not to do so, **remember to keep your password safe** as **this email address is removed from our database once the request is appoved/denied and we cannot reset it**.


  ![](../en/reg_04.png)

Then enter your password twice, checking that it meets the password rules, and click the **Continue** button to finish.

And that's it! \0/
