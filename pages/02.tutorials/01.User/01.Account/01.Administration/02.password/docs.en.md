---
title: 'Account Password Change'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: false
---

# Change your password

![](dashboard_pass.png)

Click on this option to change your current password.


!! ![](../en/note.png)<br>
!! Please read carefully the instructions and rules for changing the password and the additional steps needed to update your **Cloud** encryption key.

![](../en/pass_change_01.png)

!! ![](../en/note.png)<br>
!! Please also note that once you've changed your password you will need to make sure to change your encryption key in the **Cloud**, since all your files in **Nextcloud** are encrypted with a key that is generated from your password.
