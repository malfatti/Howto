---
title: 'Security Questions'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: true
---

# Setup your Security Questions

![](dashboard_questions.png)

In case you forget/lose your password, you can reset it without Admins intervention by setting the security questions first. To do it, click on this option.

The proccess is pretty simple.

- Click on ***Setup Security Questions***.

 ![](../en/sec_qs_01.png)

- Write the first question and its answer, then select the next two questions from the drop-down list and write the answers as well.

  ![](../en/sec_qs_02.png)

- Once the answers meet the requirements, just click ***Save Answers***.

  ![](../en/sec_qs_03.png)
