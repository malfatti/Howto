---
title: 'Request Forms'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: flase
---

# Custom Requests forms
From here you can access the forms to request additional email alias, link your personal domain or extra storage for your mailbox or cloud.

![](dashboard_forms.png)

Just click on the option you need.

![](../en/forms.png)
