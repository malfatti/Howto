---
title: Amministrazione dell'account
published: true
indexed: true
visible: true
updated:
    last_modified: "July 2019"		
taxonomy:
    category:
        - docs
    tags:
        - utente
        - amministrazione
page-toc:
    active: false
---

# Account Administration

#### [Gestisci il tuo account Disroot](ussc/)

#### [Richiedi un Alias email](alias-request)
