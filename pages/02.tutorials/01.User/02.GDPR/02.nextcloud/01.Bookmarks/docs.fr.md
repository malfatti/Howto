---
title: 'Nextcloud: Exporter les Favoris'
published: true
indexed: true
updated:
    last_modified: "July 2019"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - bookmarks
        - gdpr
visible: true
page-toc:
    active: false
---

Exporter les données de vos favoris stockées sur le cloud est très facile avec **Disroot**.

1. Connextez-vous sur [cloud](https://cloud.disroot.org)

2. Choisissez l'application "Favoris"

![](fr/select_app.gif)

3. Choisissez Paramètres (au bas de la barre latérale gauche et cliquez sur le bouton **"Exporter"**

![](fr/export.gif)
