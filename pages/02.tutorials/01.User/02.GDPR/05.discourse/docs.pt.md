---
title: Discourse: Exportar os seus posts do Fórum
published: true
visible: true
taxonomy:
    category:
        - docs

---


O Discourse, o software usado pelo Disroot para Fórum, permite-lhe exportar o conteúdo de texto de todos os seus posts para um ficheiro .csv, (que é suportado pela maioria dos programas de Folha de Cálculo/excel, Libreoffice, Openoffice, Gnumeric, Excel).

**Para exportar os seus posts do Discourse:**
- Carregue no seu Gravatar no canto superior direito do ecrã

![](pt/export_data_discourse_01.png)
- Carregue no botão com o seu nome de utilizador

![](pt/export_data_discourse_02.png)
- Carregue no botão _"Descarregar As Minhas Publicações"_

![](en/export_data_discourse_03.png)
- Carregue em _"Sim"_ quando a janela de pop-up lhe perguntar se quer descarregar os seus posts e depois carregue em _"ok"_

**NOTA:** Os dados só podem ser solicitados uma vez a cada 24 horas

![](pt/export_data_discourse_01.gif)

Irá receber uma mensagem do sistema a notificá-lo de que os seus dados estão prontos para serem descarregados, e a fornecer-lhe um link para descarregar o ficheiro .csv com a cópia dos seus posts.
Caso tenha habilitado notificações por email, receberá também um email com esta informação. Basta carregar no link para descarregar o ficheiro.

O ficheiro esta comprimido numa pasta no formato gzip. Se a pasta não abrir quando carregar nela, use as ferramentas recomendadas aqui: http://www.gzip.org/#faq4

O link estará disponível por 48 horas, depois das quais irá expirar e você terá que exportar os seus dados novamente.

Assim que descomprimir a pasta gzip pode abri-lo no seu programa de Folha de Cálulo/Excel.
