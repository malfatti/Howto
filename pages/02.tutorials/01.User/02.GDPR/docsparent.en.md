---
title: 'Export Your Personal Data / GDPR Compliance'
visible: true
published: true
indexed: true
updated:
    last_modified: "June 2020"
taxonomy:
    category:
        - docs
    tags:
        - user
        - personal data
        - gdpr
    visible: true
page-toc:
    active: false

---

#  Your data is yours:<br>How to access and export your personal data


At **Disroot**, pretty much all data collected is the one you provide when using the services (*we store your files because you choose to store files on our cloud*).

We have no interest in acquiring and collecting any extra data nor processing it to sell to advertisement companies or in any way use to monetize on it. Therefore most of the services provide you with a way to self export your data.

This chapter includes tutorials that will help you get all the data stored on services provided by **Disroot** and connected to your account.

----

## Export your data from:
- [01. Email (Roundcube)](roundcube)
- [02. Cloud (Nextcloud)](nextcloud)
  - [Files & Notes](nextcloud/files)
  - [Contacts](nextcloud/contacts)
  - [Calendars](nextcloud/calendar)
  - [News](nextcloud/news)
  - [Bookmarks](nextcloud/bookmarks)
- [03. XMPP Chat (Converse)](converse)
- [04. Diaspora*](diaspora)
- [05. Forum (Discourse)](discourse)
- [06. Board (Taiga)](taiga)
- [07. Git (Gitea)](gitea)
- [08. Hubzilla](hubzilla)

---
