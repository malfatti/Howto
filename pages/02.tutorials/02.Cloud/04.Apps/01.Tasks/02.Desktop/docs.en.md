---
title: 'Tasks: Desktop'
published: true
visible: false
updated:
        last_modified: "October 2020"
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - sync
page-toc:
    active: false
---

## Tasks on the desktop

There are several ways you can sync and work with **Nextcloud Tasks** from your desktop. Below you will find some tutorials to get it.

### Multi-platform desktop clients
 - [Thunderbird: Calendar / Contacts / Tasks sync](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)

### GNU/Linux
 - [GNOME Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
 - [KDE Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
