---
title: "Cloud Apps: Tasks"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - task
        - sync
    visible: true
page-toc:
    active: false
---

# Tasks

### [Web interface](web)
- Creating and configuring tasks

### [Desktop clients](desktop)
- Desktop clients and applications for organizing and synchronizing tasks

### [Mobile clients](mobile)
- Mobile clients and settings for organizing and synchronizing tasks
