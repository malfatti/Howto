---
title: "Calendar: Web"
published: true
indexed: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - cloud
page-toc:
    active: true
---

# Using the Calendar app via web interface

You can access your calendar app by pressing the ![](en/calendar_top_icon.png) {.inline} icon in the top bar.
*Left-bar* in the calendar window gives you an overview of your calendars, their options and general settings.

![](en/calendar_main.png)


## Create Calendar
To create a new calendar in this options panel press "*create calendar*"
A small prompt will appear where you can type the name you want to give to this new calendar and select a color for it.
Then press "*create*".

![](en/calendar_add_new.png)

You can create multiple calendars for different purposes (work, activism, etc.) by repeating this process. If you do that, you can use different colors to set them apart (last icon is the color picker which allows you to choose any color you want![](en/calendar_colorpick_icon.png)).

![](en/calendar_list.png)


## Delete, edit, Download calendar.
On the left panel, you will see your calendars listed. To the right of each calendar you will find a "more" button where you can:

- rename your calendar,
- download it
- get a url link to sync it with other devices
- delete your calendar.

![](en/calendar_edit1.png)


## Create an event
You can create a new event in your calendar by clicking in the calendars *main window* simply on the day of the event. A panel will appear on the right side, where you can fill with the information of the event.

![](en/calendar_edit_menu.png)

In this panel you can specify:

  - event title
  - start and end date
  - start and end time
  - if it's full day event or not
  - event location
  - event description

![](en/calendar_edit_menu2.png)

If you have multiple calendars, in your Disroot calendar app, you need to select to which calendar the event goes to. You can do that below the event title field.

![](en/calendar_edit_menu3.png)

You can set a reminder for the event by pressing "Reminders" and "Add"

![](en/calendar_edit_menu4.png)

You can select what type of reminder you want:

* audio
* email
* pop up

And set the time for it.

Just press the reminder you added and the options will show up.

![](en/calendar_edit_menu5.png)

You can also set if this is a repeating event or not. Just check *repeating* options.

![](en/calendar_edit_menu6.png)


## Invite people to events

You can also invite people to your event via email by:

* pressing "Attendees"
* Filing the field with the persons email address
* press enter

![](en/calendar_edit_menu7.png)

The people you invite will receive an automaticly generated email with the invitation. Any changes you make to the event will be automatically sent by email to the person you've added.

When you're done with everything, just press create at the end of the panel, and your event will appear on the screen


## Edit or delete events
To edit or delete an event you've created, just click the event on your screen, edit it and then press "update".
To delete it, you will find the big red button.

![](en/calendar_edit_menu8.png)


## Share calendars
You can share your calendars, either with another disroot user, via email, or public link.

To share with another Disroot user:

* press the the share button on the right of your calendar name
* type the username of the Disroot user that you want to share the calender with
* press enter.

![](en/calendar_share_menu1.png)

To share calendars via email or public link:

* go to the same "shared" option
* select "Share link"
* fill the email address field with the email of the person you want to share your calendar with
* press enter
* to just get the link press the chain symbol next to the mail envelope symbol

![](en/calendar_share_menu2.png)


## Import calendars
If you have a ICS file with a calendar to import, go to disroot calendar app then go to "Settings & Import" on the lower left corner of the screen.

![](en/calendar_import_menu1.png)

And select import calendar option.

![](en/calendar_import_menu2.png)
