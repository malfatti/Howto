---
title: Calendar: Mobile
published: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - cloud
        - sync
page-toc:
    active: false
---

# Calendar mobile clients

**Disroot** has the Calendar app enabled.

To setup and use **Disroot** calendars on your device, check the following tutorial:

### [Nextcloud: Mobile Clients](/tutorials/cloud/clients/mobile)
