---
title: Notes: Web
published: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
page-toc:
    active: true
---

# Notes

You can access the **Notes** app by pressing the notes icon ![](en/notes_app.png) {.inline} on the **Nextcloud** navigation bar.

## Settings

Before you start creating new notes you may want to change settings first.

In the settings (gear icon bottom left) you can choose which folder and what format the notes will be saved in. By default, notes will be saved as .txt files in the Notes folder.

![](en/notes_settings.gif)


![](en/note.png) **Note: You can always change the settings afterwards.**

## Creating notes

Once your settings are ready to go, you can start creating notes.

To do so, you just have to click on the **+** icon and start typing your note. The note will be saved automatically.

![](en/notes_creation.gif)

## Favorites

You can mark your notes as Favorite, so they will show up in the top of the list. You can do it by clicking on the star icon.<br>
Clicking the star icon again will remove the Favorite mark.

![](en/notes_favourite.png)

## Categories

It is also possible to organize your notes in categories.<br>
Go to the files app of **Nextcloud** and create a new folder with the category name you want to add in the Notes folder. After that, you can move the note file in the desired folder.

![](en/notes_categories1.gif)

Now when you move back in the Notes app you will find your notes ordered in categories.

![](en/notes_categories2.png)
