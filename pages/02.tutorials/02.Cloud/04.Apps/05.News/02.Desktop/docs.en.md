---
title: News: Desktop clients
published: true
visible: false
updated:
        last_modified: "July 2019"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - news
        - desktop
page-toc:
    active: false
---

## Desktop clients and applications

#### Multiplatform News clients (coming soon)

#### GNU/Linux News clients (coming soon)

----

#### Related How-tos
#### [GNOME: Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
#### [KDE: Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
