---
title: Cloud apps: Talk
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - talk
        - call
visible: true
page-toc:
    active: false
---

## Talk

### [Web interface](web)
- Audio/video calls, settings

### Desktop clients (coming soon)
- Talk App desktop clients

### Mobile clients (coming soon)
- Talk App, device settings
