---
title: MacOS
visible: false
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Ci-dessous vous pouvez apprendre comment intégrer Nextcloud avec votre périphérique MacOS
- [Synchronisation des calendriers](calendar-syncing)
- [Synchronisation des contacts](contact-syncing)

![](macos.jpg)
