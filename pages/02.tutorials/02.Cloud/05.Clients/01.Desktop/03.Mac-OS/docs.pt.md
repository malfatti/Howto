---
title: MacOS
visible: false
taxonomy:
    category:
        - docs
page-toc:
     active: false
---

Aqui pode aprender como integrar o Nextcloud com o seu dispositivo MacOS
- [Sincronizar Calendários](calendar-syncing)
- [Sincronizar contactos](contact-syncing)

![](macos.jpg)
