---
title: Cloud: Desktop Clients
published: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - integration
visible: true
page-toc:
    active: false
---

<br>

### [Multiplatform Desktop Clients](multiplatform)
- **Nextcloud** multiplatform clients

### [GNU/Linux](gnu-linux)
- Desktop Integration

### [MacOS](mac-os)
- MacOS device integration
