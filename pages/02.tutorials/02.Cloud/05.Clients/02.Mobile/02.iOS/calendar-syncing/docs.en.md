---
title: 'iOS - Syncing Calendars & Reminders'
updated:
        last_modified: "19 August 2018"
        app: iOS
        app_version: unknown
visible: false
page-toc:
  active: true
published: true
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - sync
        - iOS
---

# Goal:
**Sync the iOS Calendar and Reminders apps with the Disroot cloud.**

This will allow you to check, update, and remove your events and reminders from any iOS device with an internet connection. Once it runs, and nothing breaks, you'll forget it's there.

# Requirements

* Your Disroot login name
* Your Disroot password
* An iOS device (iPhone, iPad)
* A working internet connection
* 15 minutes

# Set up Calendar and Reminders Syncing

1. Go to the  **Settings** app on your device.
2. Click on '**Passwords & Accounts**'.
3. Click '**Add Account**'.
4. Click '**Other**'

![](en/ios_calendar1.png)

5. Click: '**Add CalDAV Account**'

![](en/ios_calendar2.png)

6. On the next screen enter the following, then click '**Next**':

* **Server**: `cloud.disroot.org`
* **User Name**: your username  (_without @disroot.org_)
* **Password**: your password
* **Description**: whatever you'd like

![](en/ios_calendar3.png)

!! For additional security, you may choose to use an app password instead of your main Disroot password. To do so, visit cloud.disroot.org.
!! Once logged in, click your profile picture in the top right, click Settings, then click Security.

7. On the following screen make sure Calendars and Reminders are selected then press '**Save**' You're done!

Now your calendar and reminders are added. If you now open the Calendar app you will see your calendars. If you would like, go to the Calendar and Reminders app settings to set your defaults to Disroot.
