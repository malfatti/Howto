---
title: Email Alias: Einrichtung in K-9 Mail
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - k9
        - k-9 Mail
        - android
page-toc:
    active: true
---

# Alias in K-9 Mail

Als erstes öffnest Du **K-9 Mail** und gehst in die *Kontoeinstellungen* (Konto auwählen '>' 3 Punkte unten rechts '>' Einstellungen '>' Kontoeinstellungen).

![](de/k9_alias_01.png) ![](de/k9_alias_02.png)

In den Einstellungen gehst Du auf *Nachrichten verfassen* und dort auf *Identitäten verwalten*.

![](de/k9_alias_03.png) ![](de/k9_alias_04.png)

Wenn Du nun auf die drei Punkte in der oberen rechten Ecke tippst, findest Du dort den Menüpunkt *Neue Identität erstellen*.

*(Jeder* **Disroot**-*Nutzer hat automatisch ein* benutzername@disr.it *Alias zur Verfügung)*

![](de/k9_alias_05.png) ![](k9_alias_06.png)

In der erscheinenden Eingabemaske gibst Du nun mindestens die neue Alias-Adresse ein.

![](de/k9_alias_07.png)

# Standard setzen

Um die Standard-Identität zu ändern, bleibst Du im Menü *Identitäten verwalten*. Tippe auf das von Dir bevorzugte Alias und halte es. Im nun erscheinenden Menü wählst Du *Ganz nach oben / Standard*

![](de/k9_alias_08.png)

# Email verfassen
Um eine Email mit Deinem neuen Alias zu versenden, tippe bei der Email-Erstellung auf das *Von:*-Eingabefeld und wähle das von Dir gewünschte Alias aus dem Dropdown-Menü aus.

![](de/k9_alias_09)
