---
title: "Correo: Configurar Alias"
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - configuraciones
page-toc:
    active: false
---

# Alias de correo

Una vez que hayas solicitado los alias utilizando el [formulario](https://disroot.org/es/forms/alias-request-form), necesitarás configurarlos. Aquí debajo encontrarás cómo hacerlo en varios clientes de correo.

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [Mail iOS](mailios)
