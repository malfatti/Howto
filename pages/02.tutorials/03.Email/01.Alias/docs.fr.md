---
title: Comment configurer l'alias de messagerie
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
page-toc:
    active: false
---

# Alias Email

Une fois que vous avez demandé des alias de messagerie en utilisant le [formulaire](https://disroot.org/forms/alias-request-form), vous devez les mettre en place. Ci-dessous vous trouverez comment le faire sur différents clients de messagerie.

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [Mail iOS](mailios)
