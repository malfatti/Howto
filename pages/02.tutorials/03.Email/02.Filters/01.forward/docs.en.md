---
title: "Email: Forwarding from Disroot to another email account"
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - forward
        - settings
page-toc:
    active: false
---

# Forwarding Disroot mail to another E-mail account

We understand that not everyone wants to use **Disroot** mail as their daily driver. People have their mail accounts for years and it's hard, not practical or, in some cases, even impossible for them to switch to another provider such as **Disroot**.

However, there are important information you should not miss out on.

 - Get notifications for **Forum** and **Nextcloud**. Those services are tight to **Disroot** email and you can't change it.
 - To be up to date with what's going on in **Disroot**. Every three / six weeks we're sending email to all users informing them about recent developments, new features and services. We're also sending information about scheduled downtime of services if they are longer than 20 min. We have no intention to spam you with too much information so don't worry about that.

This short how-to will show you how to forward your **Disroot** emails to your preferred email address (it should take you no more than three minutes to get this setup).

## Steps needed to forward your mail

1. **Login to webmail** [(https://mail.disroot.org)](https://mail.disroot.org)


![](en/login.jpg)


2. **Go to Settings** (hit the 'gear' icon on bottom left of the window)


![](en/webmail1.jpg)


3. Once in Settings **Hit Filter Tab.**<br>
Filters help you manage your emails. Based on your filter conditions, you can move, copy and forward any email automatically.<br>
This is quite straight forward so if you want to setup some extra filters just look around. Here we'll see how to setup forwarding filter for all your mail.


![](en/settings1.jpg)


4. **Click on "Add new Filter" icon.**
You will be presented with a dialog window that guides you through the setup.


![](en/filters1.jpg)

5. **Fill in your filter rule.**


![](en/filters2.jpg)


 - Give your filter a name
 - If no conditions specified, filter will apply to all incoming emails, which is what we want, so **do not add any conditions** for that filter.
 - From "**Action**" dropdown menu select "**Forward to**" option, and add the email address you want all emails to be forwarded to.
 - Once you're done, hit the "**Done**" button.
 - In order for the filter to be set to active you need to "**Save**" it.


![](en/filters3.jpg)

### Voila!

From now on, all your emails to **Disroot** account will be forwarded to your preferred email address. If you ever decide to actually switch to **Disroot** mail as your main email address, just simply remove that rule or alter it to your liking.
