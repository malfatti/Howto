---
title: Email: Filter konfigurieren
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - filter
        - settings
        - Einstellungen
        - Weiterleitung
page-toc:
    active: false
---

# Email-Filter

Email-Filter ermöglichen es Dir, eingehende Nachrichten automatisiert zu verwalten, indem Du sie zum Beispiel nach definierten Kriterien in verschiedene Ordner sortierst, eine Abwesenheits-Antwort einstellst und Emails automatisch ablehnst oder weiterleitest.

In diesem Kapitel wollen wir die Grundlagen basierend auf ein paar Szenarios erläutern.


### [Email-Weiterleitung](forward)
