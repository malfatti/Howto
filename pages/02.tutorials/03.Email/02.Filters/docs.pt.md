---
title: Filtros de Email
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - filters
        - settings
page-toc:
    active: false
---

# Filtros de Email

Filtros de email permitem-lhe gerir os emails que recebe de uma forma automatizada, como por exemplo mover automáticamente certos emails para uma determinada pasta utilizando determinados critérios. Criar um sistema de respostas automáticas, rejeitar emails de determinadas pessoas ou encaminhar emails automaticamente, etc.

Nesta secção iremos mostras alguns filtros básicos.
