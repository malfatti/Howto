---
title: 'Clients de bureau'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

Comment configurer votre email sur votre bureau:

## Table des matières
- [Thunderbird - client email multiplateforme](thunderbird)

![](c64.jpg)
