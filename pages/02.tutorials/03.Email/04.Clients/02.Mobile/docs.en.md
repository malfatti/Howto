---
title: 'Email: Mobile Clients'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Email clients for Mobile

How-to setup your email with your mobile device:

## Android
- [K9](k9)
- [FairEmail](fairemail)

## SailfishOS
- [Mail App](sailfishos)

## iOS
- [Mail App](ios)
