---
title: Email: Clients
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

## [Desktop Mail Client](desktop)
- [**Multiplatform**](desktop/thunderbird)
- [**GNOME** Desktop integration](desktop/gnome-desktop-integration)
- [**KDE** Desktop integration](desktop/kde-desktop-integration)


## [Mobile Mail Client](mobile)
- [**Android: K9**](mobile/k9)
- [**SailfishOS: Mail App**](mobile/sailfishos)
- [**iOS: Mail App**](mobile/ios)
