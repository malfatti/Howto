---
title: "Correo: Clientes"
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

## [Clientes de Escritorio](desktop)
- [**Multiplatforma**](desktop/thunderbird)
- [Integración con el escritorio **GNOME**](desktop/gnome-desktop-integration)
- [Integración con el escritorio **KDE**](desktop/kde-desktop-integration)


## [Clientes para Móvil](mobile)
- [**Android: K9**](mobile/k9)
- [**SailfishOS: Mail App**](mobile/sailfishos)
- [**iOS: Mail App**](mobile/ios)
