---
title: 'Hubzilla: Permissions'
updated:
page-toc:
  active: false
published: true
visible: true
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
---

# Permissions

  - [Default channel permissions limits](default_channel_permissions-limits)
  - [Channel permissions roles](channel_permission_roles)
  - [Channel permissions limits](channel_permission_limits)
  - [ACL: Access Control List](acl)
  - [Guest Access Token](guest_access_tokens)
  - [Channel roles permissions table](channel_roles)
