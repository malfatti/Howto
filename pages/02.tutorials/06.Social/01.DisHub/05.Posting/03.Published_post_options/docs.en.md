---
title: Hubzilla: Published Post options
updated:
published: false
visible: false
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
page-toc:
     active: false
---

When a post has been published, there are several options:
