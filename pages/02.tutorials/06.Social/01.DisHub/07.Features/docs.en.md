---
title: Hubzilla: Features
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
page-toc:
     active: false
---

# Hubzilla's additional Featurs:

### [Connections Filtering](connection_filtering)
### [Personal Cloud Storage](personal_cloud_storage)
### [Bookmarks](bookmarks)
### [Chat](chat)
### [Calendar](calendar)
### [Photo album](photo_album)
