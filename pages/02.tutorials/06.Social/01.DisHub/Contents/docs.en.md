---
title: Hubzilla: DisHub Guide
updated:
published: false
visible: false
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
page-toc:
     active: false
---
![](/social/dishub/en/round_logo.png)
# DisHub: Disroot's Hubzilla instance

# Contents

1. [Basics](basics)
  - [How2 start really quick?](/social/dishub/basics)
  - [User interface overview](/social/dishub/basics/user_interface)
  - [Navigation bar](/social/dishub/basics/navigation_bar)
2. [Channels](dishub/channels)
  - [What is a channel?](/social/dishub/channels)
  - [Creating a channel](/social/dishub/channels/creation)
  - [Profiles](/social/dishub/profiles)
  - [Blocking a channel](/social/dishub/channels/blocking)
  - [Ignoring a channel](/social/dishub/channels/ignoring)
  - [Archiving a channel](/social/dishub/channels/archiving)
  - [Hiding a channel](/social/dishub/channels/hiding)
3. [Connections](/social/dishub/connections)
4. [Permissions](/social/dishub/permissions)
  - [Default channel permissions limits](/social/dishub/permissions/default_channel_permissions-limits)
  - [Channel permissions roles](/social/dishub/permissions/channel_permission_roles)
  - [Channel permissions limits](/social/dishub/permissions/channel_permission_limits)
  - [ACL: Access Control List](/social/dishub/permissions/acl)
  - [Guest Access Token](/social/dishub/permissions/guest_access_tokens)
  - [Channel roles permissions table](/social/dishub/permissions/channel_roles)
5. [Posting and publishing](/social/dishub/posting)
  - [Composing](/social/dishub/posting/composing)
  - [Tags and mentions](/social/dishub/posting/tags_and_mentions)
  - [Published post options](/social/dishub/posting/published_post_options)
6. [Wiki](/social/dishub/wiki)
7. [Features](/social/dishub/features)
  - [Connections Filtering](/social/dishub/features/connection_filtering)
  - [Personal Cloud Storage](/social/dishub/features/personal_cloud_storage)
  - [Bookmarks](/social/dishub/features/bookmarks)
  - [Chat](/social/dishub/features/chat)
  - [Calendar](/social/dishub/features/calendar)
  - [Photo album](/social/dishub/features/photo_album)
8. [FAQ](/social/dishub/faq)
