---
title: "Tablero de proyectos"
subtitle: Usando Taiga
icon: fa-th
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - taiga
        - planificación
        - proyecto
page-toc:
    active: false
---

![](/home/icons/taiga.png)

# Tablero de proyectos: Taiga

**Taiga** es una herramienta de gestión de proyecto, desarrollada para programadores, diseñadores y startups cuyo funcionamiento pretende ser metodológicamente ágil. Sin embargo, puede ser aplicado a virtualmente cualquier proyecto o grupo, incluso fuera del ámbito de las IT. Genera un claro y visual resumen general del estado actual de tu proyecto a cualquier involucrado. Hace la planificación muy sencilla y te mantiene a ti y a tu equipo enfocado en las tareas. Taiga puede ajustarse a cualquier tipo de proyecto debido a su personalización; desde complejos proyectos de desarrollo de software a simples tareas domésticas. El límite es tu imaginación. Si nunca utilizaste este tipo de herramienta, te sorprenderá de qué manera tu vida puede mejorar con **Taiga**. Simplemente crea un proyecto, invita a los miembros de tu grupo, planeen tareas y pónganlas en el tablero. Resuelve quién será responsable de las tareas, sigue los progresos, comenta, decide y ve tu proyecto crecer.
