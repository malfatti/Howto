---
title: Polls
updated:
page-toc:
    active: true
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - polls
        - planning
---

# Polls

Disroot polls is an online service for appointment planning and decision making.
No registration is required. You can use it even if you don't have a Disroot account.

This service is made possible thanks to [Framadate](https://framadate.org/).

# How to create a poll

Creating a poll is very easy, just go to [poll.disroot.org](https://poll.disroot.org/). You can choose the language of your poll in the upper right corner, and choose if you want a standard poll, or a poll to schedule an event/appointment.

![](en/polls1.png)

The main differences between "Schedule an event" and "Make a standard poll" are:

* **Schedule an event**, like the name suggests is better suited to schedule meetings, actions, because it permits to set the proposals  with multiple dates and times.

* **Standard poll** is better suited do surveys. *If you want to do a poll about an event that spans over several days (with no options on starting hour), Standard Poll is also better suited.*

Once you choose which type of poll you prefer, the first step in making a poll is common to both, in the first page you need to fill:

* Your name (which will be publicly shown as the creator of the poll, choose whatever name you like)
* Poll title
* Description

And pressing in the button "Optional Parameters"  ![](en/polls07.png?resize=40,18) {.inline}

You can chose the following options:

* Poll link (personalized URL, for example: polls.disroot.org/big-decision)
* Password (to restrict the access to the poll)
* All voters can modify any vote
* Voters can modify their vote themselves
* Votes cannot be modified
* Only the poll maker can see the poll results

After that continue to step 2, by pressing the green button on the bottom of the page.

![](en/polls2.gif)

In step 2 you will have to set the choices in either your "Standard poll" or your "Schedule an event" poll.

## Set choices in Standard Poll
You can choose the number of choices you want by using the + and - buttons ![](en/polls2.png?resize=40,18) {.inline}, and fill the text you want. Once you are done press next, select the expiration date of your poll, and press "create poll"

![](en/polls3.gif)

![](en/polls4.gif)

You can also add pictures and links to the options you create, but in the case of pictures they need to be uploaded elsewhere and you need URL link to them:

![](en/polls5.gif)

## Set choices in "Schedule an event"
You can choose the number of choices you want by using the + and - buttons  ![](en/polls2.png?resize=40,18) {.inline} Select the dates in each choice and the times. Once you are done press next, select the expiration date of your poll and press "create poll".

![](en/polls6.gif)


# Share your poll

Once your poll is created you will see in the left side of the screen the public link to the poll, just copy it and send it to people you want to participate in the poll.

![](en/polls7.gif)


# Administer your poll
To administer your poll after it was created you need the "Admin link for the poll", which you can see and copy on the center part of the screen. **(don't lose it or else you wont be able to enter the admin panel)**

![](en/polls3.png)

With this admin link you can return to this page whenever you want and edit the options of the poll.

# Check the results of the poll
Just press the "Display the chart of the results" or export it to a .csv file

![](en/polls8.gif)


# Participate in the poll


* Write your name (or whatever name you want)
* Select in each choice one of three options; yes ![](en/polls4.png?resize=32,22), If need be ![](en/polls5.png?resize=31,20), and no ![](en/polls6.png?resize=32,21)
* And press save at the end

## Using the comment section
Users of the poll can use the "comment section" in the bottom of the poll page to send messages to each other.

![](en/polls9.gif)

Users can see the results of the poll (if that option was enabled by the creator of the poll) beneath the list of people that voted, or by pressing the Display the Chart of the results.
