---
title: Travail
subtitle: "Pads, Pastebin, Mumble, Polls & File Sharing"
icon: fa-file-text
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - office
        - poll
        - pastebin
        - file
        - sharing
        - disapp
page-toc:
    active: false
---

# Outil de travail

Disroot propose une série d'outils web utiles que vous voudrez peut-être essayer.

---

Pour plus de commodité, il existe également une application **Disroot** qui regroupe tous ces outils et autres services **Disroot** :

### [DisApp](../user/disapp)
